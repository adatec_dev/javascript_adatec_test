## Descripción
La prueba de javascript evalúa los conocimientos y la habilidad para resolver un problema sencillo de trabajo asíncrono con datos obtenidos de un servidor y el manejo que se le da a los mismos.

## Antes de empezar

1. Clonar el repositorio:

	1.1. Ejecutar el comando: **git clone https://ampamo@bitbucket.org/adatec_dev/javascript_adatec_test.git**
    
    1.2. Ejecutar el comando: **cd javascript_adatec_test && git fetch && git checkout [RAMA_SUMINISTRADA_PARA_LA_PRUEBA]**

2. Instalar dependencias con node package manager:

    2.1. Ejecutar el comando: **npm install -g grunt-cli**
    
	2.2. Ejecutar el comando: **npm install**

3. Correr el proyecto con grunt:

	Ejecutar el comando: **grunt**
	
4. Verificar que el sistema está corriendo:

	Acceder a: **http://localhost:9000**
	
	
## Estructura base del proyecto

-> index.html : Template para renderizar información y cargar librerías

-> tools.js:    Funciones desarrolladas para descargar y renderizar la información

-> src.js:      Archivo para desarrollar la solución al ejercicio

## Ejercicio

El sistema tiene información de usuarios vendedores y las visitas que han realizado a sus clientes. Tal como se muestra al ejecutar la función **showSellers(getExampleSellers())** que sirve de ejemplo, se requiere cargar los datos de las visitas para las siguientes fechas:

 - 04/12/2016
 - 11/12/2016
 - 18/12/2016

Para lograr lo anterior se debe utilizar la función **getSellers("aaaa-mm-dd")** que recibe por parámetro una fecha en el formato antes especificado y devuelve la información de venta de todos los usuarios para ese día. Posterior a haber obtenido la información requerida, se debe mostrar usando la función **showSellers([Array])**, la cual recibe como parámetro un array de objetos donde cada objeto debe tener la siguiente estructura:
		 
         {
            seller_code: 'código de vendedor',
            seller_name: 'nombre de vendedor',
            summaryByDates : [
                {
                    visits_date : 'Fecha de las visitas',				
                    visits_amount : 'cantidad de visitas en la fecha especificada',					
                    orders_amount : 'cantidad de pedidos en la fecha especificada',					
                    visits_without_sale_amount : 'cantidad de visitas no efectivas en la fecha especificada',					
                }				
            ]			
         }
		 
El objetivo es poder visualizar la información obtenida de los diferentes días resumida por usuario como la renderiza la función **showSellers**

## Entrega

El ejercicio debe ser entregado realizando un Pull request a este repositorio en la rama suministrada.
