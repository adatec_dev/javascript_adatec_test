
var getSellers = function (filterDate, successCallback, failureCallback) {
    $('#testMsg').show();
    $.ajax({
        method: 'GET',
        url: 'https://devback.ventasremotas.com/adatec-test/sellers/',
        data: {
            test_api_key: '00b6f0867Hl0315b4ed0483fe8347d3596cd87c0',
            filter_date : filterDate
        },
        timeout: 30000
    })
    .done(function (data) {
        $('#testMsg').hide();
        successCallback(data);
    })
    .fail(function (jqXHR, textStatus) {
        $('#testMsg').hide();
        failureCallback(jqXHR, textStatus);
    });
};

var showSellers = function (sellers) {
    var $sellersDiv = $('#sellers');

    $sellersDiv.html('');

    sellers.forEach(function (seller) {
        $sellersDiv.append(getSellerHtml(seller));
    });
};

var getSellerHtml = function (seller) {

    var html = '<div style="padding: 20px;">';

    html += '<p>Vendedor: [SELLER_CODE] - [SELLER_NAME]</p>';

    html = html.replace('[SELLER_CODE]', seller.seller_code);
    html = html.replace('[SELLER_NAME]', seller.seller_name);

    html += '<table class="table tablesorter table-bordered table-condensed ">' +
                '<thead>' +
                    '<th style="background-color: black; color:white;">Fecha</th>' +
                    '<th style="background-color: black; color:white;">Visitas</th>' +
                    '<th style="background-color: black; color:white;">Pedidos</th>' +
                    '<th style="background-color: black; color:white;">No Venta</th>' +
                '</thead>' +
                '<tbody>';


    seller.summaryByDates.forEach(function (summary) {
        var sellerData = '<tr>' +
                            '<td>[VISITS_DATE]</td>' +
                            '<td>[VISITS_AMOUNT]</td>' +
                            '<td>[ORDERS_AMOUNT]</td>' +
                            '<td>[VISITS_WITHOUT_SALE_AMOUNT]</td>' +
                        '</tr>';

        sellerData = sellerData.replace('[VISITS_DATE]', summary.visits_date);
        sellerData = sellerData.replace('[VISITS_AMOUNT]', summary.visits_amount);
        sellerData = sellerData.replace('[ORDERS_AMOUNT]', summary.orders_amount);
        sellerData = sellerData.replace('[VISITS_WITHOUT_SALE_AMOUNT]', summary.visits_without_sale_amount);

        html += sellerData;
    });

    html += '</tbody>' +
            '</table>';

    html += '</div>';

    return html;
};

var getExampleSellers = function () {
    // El valor de inicialización de sellers es de prueba
    var sellers = [
        {
            seller_code : 'PRUEBA 5',
            seller_name : 'Esto es una prueba de como debe verse',
            summaryByDates : [
                {
                    visits_date : '2019-04-05',
                    visits_amount : 20,
                    orders_amount : 18,
                    visits_without_sale_amount : 2
                },
                {
                    visits_date : '2019-04-06',
                    visits_amount : 10,
                    orders_amount : 8,
                    visits_without_sale_amount : 4
                },
                {
                    visits_date : '2019-04-10',
                    visits_amount : 4,
                    orders_amount : 8,
                    visits_without_sale_amount : 4
                }
            ]
        },
        {
            seller_code : 'PRUEBA 2',
            seller_name : 'Esto es una prueba de como debe verse',
            summaryByDates : [
                {
                    visits_date : '2019-04-05',
                    visits_amount : 40,
                    orders_amount : 20,
                    visits_without_sale_amount : 20
                },
                {
                    visits_date : '2019-04-06',
                    visits_amount : 12,
                    orders_amount : 8,
                    visits_without_sale_amount : 6
                }
            ]
        }
    ];

    return sellers;
};