$(function () {
    /*
        Cada elemento del array de sellers debe estar formado así:
        {
            seller_code: 'código de vendedor',
            seller_name: 'nombre de vendedor',
            summaryByDates : [
                {
                    visits_date : 'Fecha de las visitas',
                    visits_amount : 'cantidad de visitas en la fecha especificada',
                    orders_amount : 'cantidad de pedidos en la fecha especificada',
                    visits_without_sale_amount : 'cantidad de visitas no efectivas en la fecha especificada',
                }
            ]
         }

     */


    /*
    El siguiente método permite consultar información al backend usando la url pasada por parámetro para la fecha
    también pasada por parámetro

    getSellers('aaaa-mm-dd', function (data) {
        debugger;
    }, function (jqXHR, textStatus) {
        debugger;
    });
    */
    showSellers(getExampleSellers());
});